<!DOCTYPE html>
<html>
<head>
	<title>Skybet Tech Test</title>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="/css/style.css" />
</head>
<body>
	<div class="container">
		@if ($message = Session::get('success'))
			<br />
			<div class="alert alert-success alert-dismissable alert-fixed">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{!! $message !!}
			</div>
		@endif

		@if ($message = Session::get('warning'))
			<br />
			<div class="alert alert-warning alert-dismissable alert-fixed">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{!! $message !!}
			</div>
		@endif

		@if ($message = Session::get('error'))
			<br />
			<div class="alert alert-danger alert-dismissable alert-fixed">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				{!! $message !!}
			</div>
		@endif
		@yield('content')
	</div>

	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
</body>
</html>