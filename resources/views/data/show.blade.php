@extends('layouts.layout')

@section('content')

	<br />

	<div class="card">
		<div class="card-block">
			
			<table class="table table-condensed">
				<tr>
					<td>
						Firstname
					</td>
					<td>
						{{ $data['firstname'] }}
					</td>
				</tr>
				<tr>
					<td>
						Surname
					</td>
					<td>
						{{ $data['surname'] }}
					</td>
				</tr>
				<tr>
					<td>
						Created
					</td>
					<td>
						{{ $data['created_at'] }}
					</td>
				</tr>
				<tr>
					<td>
						Updated
					</td>
					<td>
						{{ $data['updated_at'] }}
					</td>
				</tr>
			</table>

		</div>

	</div>

	<br />

	<a href="/data" class="btn btn-primary">Go Back</a>
	<a href="/data/{{ $id }}/edit" class="btn btn-warning">Edit</a>

@endsection