@extends('layouts.layout')

@section('content')

	<br />

	<div class="card">
		<div class="card-block">
			@if(count($data) > 0)
				<table class="table table-striped table-condensed">
					<thead>
						<tr>
							<th>Firstname</th>
							<th>Surname</th>
							<th>Created</th>
							<th>Updated</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach($data as $id => $value)
							<tr>
								<td>{{ $value['firstname'] }}</td>
								<td>{{ $value['surname'] }}</td>
								<td>{{ $value['created_at'] }}</td>
								<td>{{ $value['updated_at'] }}</td>
								<td class="text-right">
									{!! Form::open(['action' => ['DataController@destroy', $id], 'method' => 'DELETE']) !!}
										<a href="/data/{{ $id }}" class="btn btn-sm btn-success">View</a>
										<a href="/data/{{ $id }}/edit" class="btn btn-sm btn-warning">Edit</a>
										{!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
									{!! Form::close() !!}						
								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@else
				<p>No data found, click "Create Data" below to add records.</p>
			@endif
		</div>

	</div>

	<br />

	<a href="/data/create" class="btn btn-primary">Create Data</a>
	<a href="/data/erase" class="btn btn-danger">Erase All Data</a>

@endsection