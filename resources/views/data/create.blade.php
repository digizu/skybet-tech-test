@extends('layouts.layout')

@section('content')

	<br />

	<div class="card">
		<div class="card-block">

			{!! Form::open(['action'=> 'DataController@store', 'method' => 'POST']) !!}
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>First name</th>
							<th>Last name</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td><input type="text" name="people[][firstname]" value="Jeff" class="form-control" required /></td>
							<td><input type="text" name="people[][surname]" value="Stelling" class="form-control" required /></td>
						</tr>
						<tr>
							<td><input type="text" name="people[][firstname]" value="Chris" class="form-control" required /></td>
							<td><input type="text" name="people[][surname]" value="Kamara" class="form-control" required /></td>
						</tr>
						<tr>
							<td><input type="text" name="people[][firstname]" value="Alex" class="form-control" required /></td>
							<td><input type="text" name="people[][surname]" value="Hammond" class="form-control" required /></td>
						</tr>
						<tr>
							<td><input type="text" name="people[][firstname]" value="Jim" class="form-control" required /></td>
							<td><input type="text" name="people[][surname]" value="White" class="form-control" required /></td>
						</tr>
						<tr>
							<td><input type="text" name="people[][firstname]" value="Natalie" class="form-control" required /></td>
							<td><input type="text" name="people[][surname]" value="Sawyer" class="form-control" required /></td>
						</tr>
					</tbody>
				</table>
				<input type="submit" value="OK" class="btn btn-success" />
			{!! Form::close() !!}

		</div>

	</div>

	<br />
	
	<a href="/data" class="btn btn-primary">Go Back</a>

@endsection