@extends('layouts.layout')

@section('content')

	<br />

	<div class="card">
		<div class="card-block">

			{!! Form::open(['action'=> ['DataController@update', $id], 'method' => 'PUT']) !!}

				<div class="form-group"> 
					{!! Form::label('firstname', 'Firstname', []) !!}
					{!! Form::text('firstname', $data['firstname'], ['class' => 'form-control']) !!}
				</div>
				<div class="form-group"> 
					{!! Form::label('surname', 'Surname', []) !!}
					{!! Form::text('surname', $data['surname'], ['class' => 'form-control']) !!}
				</div>

				{!! Form::submit('Update', ['class' => 'btn btn-success']) !!}

			{!! Form::close() !!}

		</div>

	</div>

	<br />


@endsection