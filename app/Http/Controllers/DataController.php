<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;

use Carbon\Carbon;

class DataController extends Controller
{
	private $jsonFile = 'data.json';
	private $jsonData = '';

	/**
	 * Load up JSON data file and store as jsonData
	 */
	public function __construct()
	{
		// create data file is it doesn't exist
		if (!Storage::exists($this->jsonFile)) {
			Storage::put($this->jsonFile, '');
		}

		$this->jsonData = Storage::get($this->jsonFile);
	}

	/**
	 * Returns data already stored in JSON. Returns empty collection if no data found.
	 * @return [type] [description]
	 */
	private function loadData()
	{
		return (json_decode($this->jsonData, true) === null) ? collect() : collect(json_decode($this->jsonData, true));
	}

	/**
	 * Writes JSON to the data file
	 * @param  [type] $data [description]
	 * @return [type]       [description]
	 */
	private function storeData($data)
	{
		Storage::put($this->jsonFile, $data);
	}

	private function cleanseData($data)
	{
		/**
		 * Here we're assuming that each record should contain a "firstname" and a "surname". Rather that storing a list of firstnames and surnames.
		 * If this assumtion is wrong, a list of firstnames and surnames can still be retrieved from the data this way.
		 */

		// pluck all firstnames from people collection and reject null values
		$firstnames = $data->pluck('firstname')->reject(function ($value) {
		    return empty($value);
		})->values(); // reset the values to consecutive integers

		// pluck all surnames from people collection and reject null values
		$surnames = $data->pluck('surname')->reject(function ($value) {
		    return empty($value);
		})->values(); // reset the values to consecutive integers

		// check to see if firstname & surname match counts
		if (count($firstnames) !== count($surnames)) {
			return false;
		}

		// loop through firstnames and add both firstname & last to new array based on key values
		foreach ($firstnames as $key => $value) {
			$this->return[] = [
				'firstname' => $firstnames[$key],
				'surname' => $surnames[$key],
				'created_at' => Carbon::now()->toDateTimeString(),
				'updated_at' => ''
			];
		}

		// convert back to collection and return
		return collect($this->return);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		// return data loaded from JSON

		return view('data.index', [
			'data' => $this->loadData()
		]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('data.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		
		// retrieve the "people" array and convert to collection
		$this->data = collect($request->input('people'));

		// cleanse the data (see cleanseData() function for info) 
		$this->data = $this->cleanseData($this->data);

		if (!$this->data) {
			return redirect()
				->action('DataController@create')
				->with('error', 'There was an error processing the data. Please ensure all fields are completed.');
		}

		// append new data to currently stored data
		$this->newData = $this->loadData()->merge($this->data);

		// write to json storage
		$this->storeData($this->newData);

		// return to index with success message
		return redirect()
			->action('DataController@index')
			->with('success', 'Data stored successfully.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		// load data from JSON using $id
		if (!array_key_exists($id, $this->loadData()->toArray())) {
			// $id wasn't found, throw error
			return redirect()
				->action('DataController@index')
				->with('error', 'There was an error retrieving details for that record.');
		}

		return view('data.show', [
			'id' => $id,
			'data' => $this->loadData()[$id]
		]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		// load data from JSON using $id
		if (!array_key_exists($id, $this->loadData()->toArray())) {
			// $id wasn't found, throw error
			return redirect()
				->action('DataController@index')
				->with('error', 'There was an error retrieving details for that record.');
		}

		return view('data.edit', [
			'id' => $id,
			'data' => $this->loadData()[$id]
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id)
	{
		// Load data from JSON & convert to array
		$data = $this->loadData()->toArray();

		// Find item based on $id and replace the values. Be sure not to replace the entire record so we can retain the created_at timestamp.
		$data[$id]['firstname'] = $request->input('firstname');
		$data[$id]['surname'] = $request->input('surname');
		$data[$id]['updated_at'] = Carbon::now()->toDateTimeString();

		// store data back to file
		$this->storeData(collect($data));

		// return to view record with success message		
		return redirect()
			->action('DataController@show', ['id' => $id])
			->with('success', 'Record updated successfully.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		// Load data from JSON & convert to array
		$data = $this->loadData()->toArray();

		if (!array_key_exists($id, $data)) {
			// $id wasn't found, throw error
			return redirect()
				->action('DataController@index')
				->with('error', 'There was an error retrieving details for that record.');
		}

		// remove record from array
		unset($data[$id]);

		// store data back to file
		$this->storeData(collect($data));

		// return to view record with success message		
		return redirect()
			->action('DataController@index')
			->with('success', 'Record deleted successfully.');

	}

	public function erase()
	{
		// empty the JSON file
		$this->storeData(collect());

		// return to view record with success message		
		return redirect()
			->action('DataController@index')
			->with('success', 'All data deleted successfully.');
	}
}
