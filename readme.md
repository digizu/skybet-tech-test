## About Project

This project was built in response to the Skybet Tech Test. https://github.com/skybet/tech-test.

## Demo

A demo of this project can be viewed here - http://techtest.rossmc.co/.

## Installation

- Clone repo
- Run `composer update` to install packages
- Duplicate .env.example and rename to .env if file isn't generated.


## Controllers

There is only one controller used for this project found in "app/Http/Controllers/DataController.php". This handles all the different methods for manipulating the data.

## Views

There are 4 views used for displaying & inputting data. These are stored in "resources/views/data".

## Notes

All data is stored in a json file situated in the storage folder outside of the root. This can be found in "storage/app/data.json".